"""checker script. Usage: put x.py in the solutions/ folder and run python checker.py"""

from importlib import import_module
import sys
from pathlib import Path
import pandas as pd
from natsort import natsorted
import numpy as np
from datetime import datetime

solutions_dir = Path(__file__).parents[1] / "solutions"
gt_dir = Path(__file__).parents[1] / "ground_truth"
sys.path.append(str(solutions_dir))

def main():
    sols = [x.stem for x in solutions_dir.iterdir() if x.suffix == ".py" and x.name != "template.py"]
    gts = [x.stem for x in gt_dir.iterdir() if x.suffix == ".csv"]
    all = natsorted(list(set([*sols, *gts])))
    assert len(sys.argv) <= 2, "Usage: python checker.py [N]"

    if len(sys.argv) == 2:
        assert sys.argv[1] in all, f"{sys.argv[1]} not in {all}"
        all = [sys.argv[1]]

    now = datetime.now().strftime("%Y%d%m%H%M%S")
    logs_dir = (Path(__file__).parents[1] / f"logs/{now}")

    print("== Checker ==")
    for sol in all:
        if not (solutions_dir / (sol + ".py")).exists():
            print(f"{sol: <5} -- has GT, but no solution. Solve it, bro")
            continue

        try:
            y: pd.DataFrame = getattr(import_module(sol), "main")()
        except Exception as e:
            print(f"{sol: <5} -- ERROR")
            logs_dir.mkdir(exist_ok=True, parents=True)
            open(logs_dir / f"{sol}.txt", "w").write(str(e))
            continue

        try:
            gt: pd.DataFrame = pd.read_csv(gt_dir / f"{sol}.csv", index_col=0)
        except Exception:
            print(f"{sol: <5} -- has solution, but no GT. Scrape more data :)")
            continue

        try:
            y = y.fillna(-9999999999)
            gt = gt.fillna(-9999999999)
            diffs = (y != gt)
            res = (diffs.sum() == 0).all()
        except Exception as e:
            print(f"{sol: <5} -- ERROR.")
            logs_dir.mkdir(exist_ok=True, parents=True)
            open(logs_dir / f"{sol}.txt", "w").write(str(e))
            continue

        if res:
            print(f"{sol: <5} -- OK")
        else:
            rows = np.where(diffs.sum(1) != 0)[0]
            logs_dir.mkdir(exist_ok=True, parents=True)
            open(logs_dir / f"{sol}.txt", "w").write(f"{y.iloc[rows]} vs\n {gt.iloc[rows]}")
            print(f"{sol: <5} -- NOT OK")


if __name__ == "__main__":
    main()
