"""
Use this template to solve all the problems. Basically this script must return the results dataframe. This result will
be compared against the ground truth found in ground_truth/ directory. Name the file <index>.py with index being the
number of the problem. Example: 1.py for 1st problem.

Task: Show first name, last name, and gender of patients who's gender is 'M'
"""

from pathlib import Path
import pandas as pd

def main() -> pd.DataFrame:
    dbs_dir = Path(__file__).parents[1] / "dbs"
    patients = pd.read_csv(dbs_dir / "patients.csv", index_col=0)
    solution = patients.loc[pd.isna(patients["allergies"]), ["first_name", "last_name"]].reset_index(drop=True)
    return solution


if __name__ == "__main__":
    _ = main()
