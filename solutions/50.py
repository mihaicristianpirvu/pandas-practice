"""
Use this template to solve all the problems. Basically this script must return the results dataframe. This result will
be compared against the ground truth found in ground_truth/ directory. Name the file <index>.py with index being the
number of the problem. Example: 1.py for 1st problem.
"""

from pathlib import Path
import pandas as pd

def main() -> pd.DataFrame:
    dbs_dir = Path(__file__).parents[1] / "dbs"
    admissions = pd.read_csv(dbs_dir / "admissions.csv", index_col=0)
    cnt_admissions = admissions.groupby("admission_date").count()["discharge_date"]
    diffs = [None, *cnt_admissions.values[1:] - cnt_admissions.values[0:-1]]
    cnt_admissions = cnt_admissions.to_frame().rename(columns={"discharge_date": "admission_day"})
    cnt_admissions["admission_count_change"] = diffs
    solution = cnt_admissions.reset_index()
    return solution


if __name__ == "__main__":
    _ = main()
