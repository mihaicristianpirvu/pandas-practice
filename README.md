https://www.sql-practice.com/ but in pandas.

See [solutions/template.py](solutions/template.py) and [solutions/1.py](solutions/1.py) for examples. Put all the solutions in that dir with the name `<problem_index>.py`

Run `python scripts/checker.py` to run all the solutions.

# Problems

## Easy
1. Show first name, last name, and gender of patients who's gender is 'M'
2. Show first name and last name of patients who does not have allergies. (null)
3. Show first name of patients that start with the letter 'C'
4. Show first name and last name of patients that weight within the range of 100 to 120 (inclusive)
5. Update the patients table for the allergies column. If the patient's allergies is null then replace it with 'NKA'
6. Show first name and last name concatinated into one column to show their full name.
7. Show first name, last name, and the full province name of each patient.
Example: 'Ontario' instead of 'ON'
8. Show how many patients have a birth_date with 2010 as the birth year.
9. Show the first_name, last_name, and height of the patient with the greatest height.
10. Show all columns for patients who have one of the following patient_ids:
1,45,534,879,1000
11. Show the total number of admissions
12. Show all the columns from admissions where the patient was admitted and discharged on the same day.
13. Show the patient id and the total number of admissions for patient_id 579.
14. Based on the cities that our patients live in, show unique cities that are in province_id 'NS'?
15. Write a query to find the first_name, last name, and birth date of patients who has height greater than 160 and weight greater than 70
16. Write a query to find list of patients first_name, last_name, and allergies from city 'Hamilton' where allergies is not null
17. Based on cities where our patient lives in, write a query to display the list of unique city starting with a vowel (a, e, i, o, u). Show the result order in ascending by city.

## Medium
18. Show unique birth years from patients and order them by ascending.
19. Show unique first names from the patients table which only occurs once in the list. For example, if two or more people are named 'John' in the first_name column then don't include their name in the output list. If only 1 person is named 'Leo' then include them in the output.
20. Show patient_id and first_name from patients where their first_name start and ends with 's' and is at least 6 characters long.
21. Show patient_id, first_name, last_name from patients whos diagnosis is 'Dementia'.
Primary diagnosis is stored in the admissions table.
22. Display every patient's first_name.
Order the list by the length of each name and then by alphbetically
23. Show the total amount of male patients and the total amount of female patients in the patients table.
Display the two results in the same row.
24. Show first and last name, allergies from patients which have allergies to either 'Penicillin' or 'Morphine'. Show results ordered ascending by allergies then by first_name then by last_name.
25. Show patient_id, diagnosis from admissions. Find patients admitted multiple times for the same diagnosis.
26. Show the city and the total number of patients in the city. Order from most to least patients and then by city name ascending.
27. Show first name, last name and role of every person that is either patient or doctor. The roles are either "Patient" or "Doctor"
28. Show all allergies ordered by popularity. Remove 'NKA' and NULL values from query.
29. Show all patient's first_name, last_name, and birth_date who were born in the 1970s decade. Sort the list starting from the earliest birth_date.
30. We want to display each patient's full name in a single column. Their last_name in all upper letters must appear first, then first_name in all lower case letters. Separate the last_name and first_name with a comma. Order the list by the first_name in decending order. X: SMITH,jane
31. Show the province_id(s), sum of height; where the total sum of its patient's height is greater than or equal to 7,000.
32. Show the difference between the largest weight and smallest weight for patients with the last name 'Maroni'
33. Show all of the days of the month (1-31) and how many admission_dates occurred on that day. Sort by the day with most admissions to least admissions.
34. Show the all columns for patient_id 542's most recent admission_date.
35. Show patient_id, attending_doctor_id, and diagnosis for admissions that match one of the two criteria:
- patient_id is an odd number and attending_doctor_id is either 1, 5, or 19.
- attending_doctor_id contains a 2 and the length of patient_id is 3 characters.
36. Show first_name, last_name, and the total number of admissions attended for each doctor.
Every admission has been attended by a doctor.
37. For each doctor, display their id, full name, and the first and last admission date they attended.
38. Display the total amount of patients for each province. Order by descending.
39. For every admission, display the patient's full name, their admission diagnosis, and their doctor's full name who diagnosed their problem.
40. display the number of duplicate patients based on their first name and last name.
Ex: A patient with an identical name can be considered a duplicate.
41. Display patient's full name, height in the unit feet rounded to 1 decimal, weight in the unit pounds rounded to 0 decimals, birth_date, gender non abbreviated. Convert CM to feet by dividing by 30.48. Convert KG to pounds by multiplying by 2.205.

## Hard
42. Show all of the patients grouped into weight groups.
Show the total amount of patients in each weight group.
Order the list by the weight group decending.
For example, if they weight 100 to 109 they are placed in the 100 weight group, 110-119 = 110 weight group, etc.
43. Show patient_id, weight, height, isObese from the patients table.
Display isObese as a boolean 0 or 1.
Obese is defined as weight(kg)/(height(m)2) >= 30.
- weight is in units kg.
- height is in units cm.
44. Show patient_id, first_name, last_name, and attending doctor's specialty.
Show only the patients who has a diagnosis as 'Dementia' and the doctor's first name is 'Lisa'
Check patients, admissions, and doctors tables for required information.
45. All patients who have gone through admissions, can see their medical documents on our site. Those patients are given a temporary password after their first admission. Show the patient_id and temp_password.
The password must be the following, in order:
- patient_id
- the numerical length of patient's last_name
- year of patient's birth_date
46. Each admission costs $50 for patients without insurance, and $10 for patients with insurance. All patients with an even patient_id have insurance.
Give each patient a 'Yes' if they have insurance, and a 'No' if they don't have insurance. Add up the admission_total cost for each has_insurance group.
47. Show the provinces that has more patients identified as 'M' than 'F'. Must only show full province_name
48. We are looking for a specific patient. Pull all columns for the patient who matches the following criteria:
- First_name contains an 'r' after the first two letters.
- Identifies their gender as 'F'
- Born in February, May, or December
- Their weight would be between 60kg and 80kg
- Their patient_id is an odd number
- They are from the city 'Kingston'
49. Show the percent of patients that have 'M' as their gender. Round the answer to the nearest hundreth number and in percent form.
50. For each day display the total amount of admissions on that day. Display the amount changed from the previous date.
51. Sort the province names in ascending order in such a way that the province 'Ontario' is always on top.